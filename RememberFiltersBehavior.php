<?php

namespace diggindata\rememberfilters;

use Yii;
use yii\base\Behavior;

class RememberFiltersBehavior extends Behavior
{
    /**
     * Array that holds any default filter value like array('active'=>'1')
     *
     * @var array
     */
    public $defaults=array();
    /**
	 * Holds a custom stateId key 
	 *
	 * @var string
	 */
    private $_rememberScenario=null;

    private function getStatePrefix() {
	    $modelName = basename(str_replace('\\', '_', get_class($this->owner)));
	    if ($this->_rememberScenario!=null) {
	        return $modelName.$this->_rememberScenario;
	    } else {
	        return $modelName;
	    }
    }

    public function load($data, $formName = null)
    {
        \Yii::info('RememberFiltersBehavior/load', 'behavior');
        return $this->owner->load($data, $formName);
    }
    public function doReadSave($params) 
    {

        // store also sorting order
        $key = 'sort';
        if(!empty($_GET[$key])){
            Yii::$app->getSession()->set($this->getStatePrefix() . '_sort', $_GET[$key]);
        }else {
            $val = Yii::$app->getSession()->get($this->getStatePrefix() . '_sort');
            if(!empty($val))
                $_GET[$key] = $val;
        }
        
        // store active page in page
        $key = 'page';
        if (!empty($_GET[$key])) {
            Yii::$app->session->set($this->getStatePrefix() . '_page', $_GET[$key]);
        } elseif (!empty($_GET["ajax"])) {
            // page 1 passes no page number, just an ajax flag
            Yii::$app->getSession->set($this->getStatePrefix() . '_page', 1);
        } else {
            $val = Yii::$app->getSession()->get($this->getStatePrefix() . '_page');
            if(!empty($val)) {
                \yii\helpers\VarDumper::dump($val, 10, true);
                $_GET[$key] = $val;
            }
        }

        $key = basename(get_class($this->owner));
        if (isset($_GET[$key])) {
            $this->owner->attributes = $_GET[$key];
            $this->saveSearchValues();
        } else {
            $this->readSearchValues();
        }        
        return $params;
    }

    private function readSearchValues() 
    {
        $modelName = $this->getStatePrefix();
        $attributes = $this->owner->safeAttributes();
        // set any default value
        if (is_array($this->defaults) && (null==Yii::$app->getSession()->get($modelName . '_defaultsSet'))) {
            foreach ($this->defaults as $attribute => $value) {
                if (null == (Yii::$app->getSession()->get($modelName . '_' . $attribute))) {
                    Yii::$app->getSession()->set($modelName . '_' . $attribute, $value);
                }
            }
            Yii::$app->getSession()->set($modelName . '_defaultsSet', 1);
        }
        
        // set values from session
        foreach ($attributes as $attribute) {
            if (null != ($value = Yii::$app->getSession()->get($this->getStatePrefix() . '_' . $attribute, null))) {
                try
                {
                    $this->owner->$attribute = $value;
                }
                catch (Exception $e) {
                }
            }
        }
    }    

    private function saveSearchValues() 
    {
        $attributes = $this->owner->safeAttributes();
        foreach ($attributes as $attribute) {
            if (isset($this->owner->$attribute)) {
                Yii::$app->getSession()->set($this->getStatePrefix() . '_' . $attribute, $this->owner->$attribute);
            } else {
                Yii::$app->getSession()->set($this->getStatePrefix() . '_' . $attribute, '');
            }
        }
    }

}
